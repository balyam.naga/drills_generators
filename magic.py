def gen_range(start, stop, step):
    while start <= stop:
        yield start
        start = start+step        

x = gen_range(10, 20, 1)

for i in x:
    print(i)

#-------------------------------------------------------
#Didnt understand this zip func well stuck in it. took from zip documentation
def zipf(*iterables):     
    sentinel = object()
    iterators = [iter(it) for it in iterables] 
    count = 0
    while iterators:
        result = []
        count = count+1
        for it in iterators:  
            elem = next(it, sentinel)
            print(elem)
            if elem is sentinel:
                return
            result.append(elem)
             
        yield tuple(result)




d = zipf("abcd","1234")
print(list(d))

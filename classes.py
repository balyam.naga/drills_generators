
import random


class Bank:
    def __init__(self):
        self.accounts = []

    def add_account(self, account):
        self.accounts.append(account)

    def __account__(self):
        return self.accounts

class BankAccount:
    def __init__(self, id, balance):
        self.id = id
        self.balance = balance

    def __lt__(self, other):
        if self.balance > other.balance:  
            return True
        else:
            return False  
    
    def __check__(self, other):

        if self.id == other.id and self.balance == other.balance:
            return True
        else:
            return False    
          
    def __repr__(self):
        return "BankAccount(id={}, balance={})".format(self.id, self.balance)


if __name__ == "__main__":
    accounts = [BankAccount(id=i, balance=random.randint(1, 10000)) for i in range(10)]
    for i in range(len(accounts)):
        for j in range(len(accounts)-i-1):
            if accounts[j].__lt__(accounts[j+1]):
                temp = accounts[j]
                accounts[j] = accounts[j+1]
                accounts[j+1] = temp
    print(accounts)
    b1 = BankAccount(1, 2400)
    b2 = BankAccount(1, 2400)
    b3 = BankAccount(2, 2400)

    print(b1.__check__(b2))
    print(b2.__check__(b3))
    
    bank = Bank()
    for i in range(100):
        bank.add_account(BankAccount(id=i, balance=i * 100))
    for account in bank.__account__():
        print(account)


